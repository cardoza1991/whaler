Given the requirements and assumptions, we will need to create a Python application that uses web scraping to track whale activities in the crypto market and sends email notifications. We will use BeautifulSoup for web scraping, smtplib for sending emails, and schedule for scheduling tasks.

Here are the core classes, functions, and methods that will be necessary:

1. `WhaleTracker` class: This class will be responsible for tracking whale activities. It will have the following methods:
   - `__init__`: Initializes the tracker with the necessary parameters.
   - `get_whale_activities`: Scrapes the web for whale activities.
   - `send_email`: Sends an email notification with the whale activities.

2. `main` function: This function will be the entry point of the application. It will create an instance of `WhaleTracker` and schedule the tracking task.

Now, let's write the code for each file.

**requirements.txt**
